=======
 Lemme
=======

Welcome to the Lemme project of the `GNU Mailman`_ suite.  Lemme is an
authenticating REST proxy which can be exposed on the internet and allows
users to script their mailing lists, subscriptions, and more.

The `Mailman core`_ exposes a administrative REST API for privileged use by
trusted clients.  These include Postorius_ as the web ui, and HyperKitty_ as
the archiver.  However the core's REST API should never be exposed on the
public internet because by design it doesn't support authentication or
permissions.

Lemme is the component of the Mailman suite that *does* provide authentication
and permissions.  On the back-end, it speaks to the core's administrative REST
API.  On the front-end, it exposes that REST API, but enforces permissions
such that only authenticated users with the right roles are allowed to perform
certain operations.

Lemme is a new project, based in large part on Andrew Stuart's Mailmania_.
Lemme is named after famous bass player and founder of Motörhead_,  `Ian
Fraser "Lemmy" Kilmister`_.


Project details
===============

* Project home: https://gitlab.com/mailman/lemme
* Report bugs at: https://gitlab.com/mailman/lemme/issues
* Fork the code: https://gitlab.com/mailman/lemme.git
* Documentation: http://lemme.readthedocs.io/en/latest/
* PyPI: https://pypi.python.org/pypi/lemme


Copyright
=========

Copyright 2016 by the Free Software Foundation, Inc.

This file is part of GNU Mailman.

GNU Mailman is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free Software
Foundation, either version 3 of the License, or (at your option) any later
version.

GNU Mailman is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with
GNU Mailman.  If not, see <http://www.gnu.org/licenses/>.


.. _`GNU Mailman`: http://docs.mailman3.org/en/latest/
.. _`Mailman core`: http://mailman.readthedocs.io/
.. _Postorius: http://postorius.readthedocs.io/en/latest/
.. _HyperKitty: http://hyperkitty.readthedocs.io/en/latest/
.. _Mailmania: https://gitlab.com/astuart/mailmania
.. _Motörhead: https://en.wikipedia.org/wiki/Mot%C3%B6rhead
.. _`Ian Fraser "Lemmy" Kilmister`: https://en.wikipedia.org/wiki/Lemmy
